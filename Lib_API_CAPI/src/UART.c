/*
 * UART.c
 *
 *  Created on: 21/06/2011
 *      Author: Alejandro
 */

#include "CAPI_DEFINITIONS.h"
#include "UART.h"
#include "ColaCircular.h"
#include "string.h"

res_t UARTputc(UART * uart, char c)
{
	ColaCircular * cola = &uart->colaTx;
	cola_t dato;
	if(ColaLlena(cola))
		return CAPI_ERROR;
	dato.EsComando = false;
	dato.dato = c;
	ColaPoner(cola, &dato);

	if(!uart->TxEnCurso){
		UART_IntTransmision(uart);	// Pongo en marcha la transmision
	}

	return CAPI_EXITO;
}

//cola_t UARTgetc(UART * uart)
res_t UARTgetc(UART * uart)
{
	ColaCircular * cola = &uart->colaRx;
	cola_t dato;
	if(ColaVacia(cola))
		return CAPI_ERROR;

	return ColaSacar(cola, &dato);
}

res_t UARTputs(UART * uart, const char * s)
{
	ColaCircular * cola = &uart->colaTx;
	cola_t dato;
	if(ColaDisponible(cola) < strlen(s))
		return CAPI_ERROR;
	dato.EsComando = false;
	while(*s)
	{
		dato.dato = *s++;
		ColaPoner(cola, &dato);
	}
	if(!uart->TxEnCurso){
		UART_IntTransmision(uart);	// Pongo en marcha la transmision
	}

	return CAPI_EXITO;
}

res_t UARTgets(UART * uart, char * s)
{
	//FIXME: No se chequea que el buffer pueda contener el string que haya en la cola,
	//si la misma no contiene un \0 va a pisar el buffer que recibio
	ColaCircular * cola = &uart->colaRx;
	cola_t dato;
	dato.EsComando = false;
	ColaSacar(cola, &dato);
	*s = dato.dato ;
	while(*s){
		s++;
		ColaSacar(cola, &dato);
		*s = dato.dato ;
//		*s = ColaSacar(cola);
	}
	return CAPI_EXITO;
}
