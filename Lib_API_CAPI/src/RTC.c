/*
 * RTC.c
 *
 *  Created on: May-2013
 *  	Author: Pablo Montalti
 */

#include "RTC.h"

int RTCInicializado = 0;

void (*RTCAlarm_Handler)(void); //Callbacks a la rutinas a ejecutar para las fuentes de interrupción
void (*RTCCounter_Handler)(void);

int RTCset_date (RTCDate * date, void(*callback_alarm_handler)(void))
{
	RTCInit();

	//RTCCounter_Handler = callback_alarm_handler;
	RTCconfigure_date(date);

	return 1;
}

int RTCset_alarm (RTCDate * alarm_date, void(*callback_alarm_handler)(void))
{

	RTCAlarm_Handler = callback_alarm_handler;
	RTCconfigure_alarm(alarm_date);
}

