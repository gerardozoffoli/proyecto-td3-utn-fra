################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/AD7793.c \
../src/Communication.c \
../src/Main.c \
../src/PANTALLA.c \
../src/RELOJ.c \
../src/TARJETA_SD.c \
../src/cr_startup_lpc176x.c \
../src/menus.c \
../src/utiles.c 

OBJS += \
./src/AD7793.o \
./src/Communication.o \
./src/Main.o \
./src/PANTALLA.o \
./src/RELOJ.o \
./src/TARJETA_SD.o \
./src/cr_startup_lpc176x.o \
./src/menus.o \
./src/utiles.o 

C_DEPS += \
./src/AD7793.d \
./src/Communication.d \
./src/Main.d \
./src/PANTALLA.d \
./src/RELOJ.d \
./src/TARJETA_SD.d \
./src/cr_startup_lpc176x.d \
./src/menus.d \
./src/utiles.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -DLIB_MCU_LPC134x=134 -DLIB_MCU_LPC176x=176 -D__TARGET_PORT=LIB_MCU_LPC176x -DFreeRTOSv7p0p1=701 -D__USE_FREERTOS=FreeRTOSv7p0p1 -I"C:\TD3\proyecto-td3-utn-fra\Smart_Fish_Bowl_2017\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_LCD_Texto\inc" -I"C:\TD3\proyecto-td3-utn-fra\CMSISv2p00_LPC17xx\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_MCU_LPC176x\vendor drivers\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_MCU_LPC176x\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_API_CAPI\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FAT_ElmChanFS\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_Contenedores\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FreeRTOS_7p0p1\FreeRTOS_include" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FreeRTOS_7p0p1\FreeRTOS_portable" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FreeRTOS_7p0p1\demo_code" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

src/cr_startup_lpc176x.o: ../src/cr_startup_lpc176x.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -D__USE_CMSIS=CMSISv2p00_LPC17xx -DLIB_MCU_LPC134x=134 -DLIB_MCU_LPC176x=176 -D__TARGET_PORT=LIB_MCU_LPC176x -DFreeRTOSv7p0p1=701 -D__USE_FREERTOS=FreeRTOSv7p0p1 -I"C:\TD3\proyecto-td3-utn-fra\Smart_Fish_Bowl_2017\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_LCD_Texto\inc" -I"C:\TD3\proyecto-td3-utn-fra\CMSISv2p00_LPC17xx\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_MCU_LPC176x\vendor drivers\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_MCU_LPC176x\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_API_CAPI\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FAT_ElmChanFS\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_Contenedores\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FreeRTOS_7p0p1\FreeRTOS_include" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FreeRTOS_7p0p1\FreeRTOS_portable" -I"C:\TD3\proyecto-td3-utn-fra\Lib_FreeRTOS_7p0p1\demo_code" -O0 -Os -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"src/cr_startup_lpc176x.d" -o  "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


