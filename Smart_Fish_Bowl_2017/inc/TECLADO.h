/*
 * TECLADO.h
 *
 *  Created on: 30/9/2015
 *      Author: Gerardo
 */

#ifndef INC_TECLADO_H_
#define INC_TECLADO_H_

#include "LPC17xx.h"
#include <cr_section_macros.h>
#include <NXP/crp.h>
#include "string.h"
#include "stdio.h"

// TODO: insert other include files here
/*Includes del FreeRTOS*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"
#include "basic_io.h"

/*Includes de la C-API*/
#include "GPIO.h"
#include "BUS.h"
#include "lpc_176X_PinNames.h"
#include "lpc_176X_PeriphNames.h"

// TODO: insert other definitions and declarations here
GPIO Columna1;
GPIO Columna2;
GPIO Columna3;
GPIO Columna4;
GPIO Fila1;
GPIO Fila2;
GPIO Fila3;
GPIO Fila4;

/* Número de items que la cola puede almacenar */
#define mainQUEUE_LENGTH					( 3 )

#define REFRESH_RATE_ms 500
#define TIEMPO_DE_DIAGNOSTICO_ms 2000
#define SCAN_RATE_ms 5
#define BOARD_SCAN_RATE_ms 	5
#define TIEMPO_DE_DEBOUNCE_ms 20
#define TIEMPO_MAX_PRESIONADO_3seg 3000
#define CANTIDAD_TECLAS 16

//Estados de la máquina
#define NO_OPRIMIDO	0
#define DEBOUNCE	1
#define VALIDAR		2
#define OPRIMIDO	3

#define NO_KEY    0
#define TECLA_0   1
#define TECLA_1   2
#define TECLA_2   3
#define TECLA_3   4
#define TECLA_4   5
#define TECLA_5   6
#define TECLA_6   7
#define TECLA_7   8
#define TECLA_8   9
#define TECLA_9   10
#define TECLA_F1  11
#define TECLA_F2  12
#define TECLA_UP  13    //#define TECLA_F3  13
#define TECLA_DOWN  14  //#define TECLA_F4  14
#define TECLA_AST 15
#define TECLA_NUM 16



#endif /* INC_TECLADO_H_ */

