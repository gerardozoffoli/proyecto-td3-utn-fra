/*
 * PANTALLA.h
 *
 *  Created on: 1/10/2015
 *      Author: Gerardo
 */

#ifndef PANTALLA_H_
#define PANTALLA_H_

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>
#include "string.h"

// TODO: insert other include files here
/*Includes del FreeRTOS*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/*Includes de la C-API*/
#include "GPIO.h"
#include "lpc_176X_PinNames.h"
#include "lpc_176X_PeriphNames.h"
#include "LCD_text.h"

// Definiciones constantes
#define FILAS		2
#define COLUMNAS	16

//Estructura de información para mostrar en pantalla
typedef struct _LCDMsg
{
	int 	fila;
	char* 	mensaje;
}LCDMsg;

// Estructura para manejo de LCD
LCDtext DisplayLCD;

/*Prototipos de funciones utilizadas*/
void TareaLCD (void *pvParameters);

#endif /* PANTALLA_H_ */
