/*
 * menus.h
 *
 *  Created on: /10/2015
 *      Author: Walter Mussin
 *  Objetivo:
 *  	Agrupo todas las definiciones y prototipos necesarios para manejar los menus.
 *
 */

#ifndef MENUS_H_
#define MENUS_H_
#include <stdint.h>
#include <stdio.h>
#include <string.h>
#include "LCD_Definitions.h"
#include "LCD_text.h"


#define MAXELMENTExMENU		7

#ifndef NULL
  #define NULL 0
#endif

typedef enum {PADRE, SUBMENU_1, SUBMENU_2}contexto;  // Identificadores para Handler,

typedef enum {SUBIR, BAJAR, SELECCION, VOLVER, MEDICION, HORA, FECHA, CALIBRAR, SD, EJECUTAR} Eventos; // Eventos que se envian al dispache del menu y tambien deberia ser utilizados para las funciones.

typedef enum {CREADO, SIN_ERROR, NO_SEPUEDE_CREAR, NO_HAY_MEMORIA_DISPONIBLE,
	          NO_DISPONIBLE, MEMORIA_LLENA, ENTRADA_UTILIZADA     }EstadoResultado;

// defino los valores que son utilizados internamente para realizar las funciones internas.
typedef enum { ADELANTA, ATRAS}Util;

// esta estrucura describe cada elemento de los menus
struct __MenuItem
{
	bool esUltimo;
	char Titulo[16];      // Titulo/ nombre del item en el menu.
	struct __MenuItem *Hijos;      // Si tiene un sub menu se apunta desde aca, si es NULL no hay submenu.
	void (*hEvento_Accion)(Eventos EventId, char param2); // Puntero a una funcion que realisa la accion segun los eventos.
};

typedef struct __MenuItem MenuItem;

typedef struct
{
	contexto DondeEstoy;   			// Para saber rapidamente en que submenu estoy.
	uint16_t Indice,       			// indice actual del menu donde estoy
			 Anterior,
			 Elementos;  			// cantidad de Elementos que tiene el menu actual.
			 MenuItem *Menus;       // lista de menu
}MenuHandler;



void VolverMenuAnterior(void);	// Vuelve al menu anterior
void Imprimir_menu(void);		// Imprime en pantalla el menu
void EventDispatchMenu(Eventos ID_Event, char param2);  //Punto de entrada para procesar todos los eventos que afectan a los menus.

void Subir_Menu(void);			//utilizado para ir subiendo en el menu.
void Bajar_Menu(void);			// utilizado para ir bajando en le menu
void SelectMenu(Eventos ID_Event);

void PrintReturnMsg(void);

void InitMenus( LCDtext* HandleDisplay );

void CrearMenuPadres( MenuItem *Array, uint16_t Leng );
EstadoResultado CrearSubMenu_1( uint16_t PadreIdx, MenuItem Array[],uint16_t Leng );
EstadoResultado CrearSubMenu_2( uint16_t PadreIdx, uint16_t HijoIdx, MenuItem Array[],uint16_t Leng );

EstadoResultado AgregarItemPadres( char* Nombre, uint8_t NombreLeng, void (*FuncionEvento)(Eventos EventId, char param2) );

EstadoResultado AgregarItemSub_1(uint16_t PadreIdx, char* Nombre, uint8_t NombreLeng, void (*func)(Eventos, char) );
EstadoResultado AgregarItemSub_2(uint16_t PadreIdx, uint16_t HijoIdx, char* Nombre, uint8_t NombreLeng, void (*func)(Eventos, char) );
EstadoResultado AgregarItem(MenuItem* Current, char* Nombre, uint8_t NombreLeng, void (*func)(Eventos, char) );
//EstadoResultado AgregarItem( MenuItem* Current, char* Nombre, uint8_t NombreLeng, void (*func)(Eventos, char ) );

EstadoResultado PrimerItemLibre(MenuHandler* Current, uint16_t* PosLibre);

void CreardorMenus(void);
void TareaMenu(void *pvParameters);
void TareaAutoDispatcher(void *pvParameters);
void ToggeLed(void);
void VerFechaHora (void);
void SystemToDefault (Eventos EventID, char param2);


#endif /* MENUS_H_ */
