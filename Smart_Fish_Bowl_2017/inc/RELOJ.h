/*
 * RELOJ.h
 *
 *  Created on: 30/9/2015
 *      Author: Gerardo
 */

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>
#include "string.h"

// TODO: insert other include files here
/*Includes del FreeRTOS*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/*Includes de la C-API*/
#include "GPIO.h"
#include "lpc_176X_PinNames.h"
#include "lpc_176X_PeriphNames.h"
#include "RTC.h"
#include "lpc17xx_rtc.h"
#include "LCD_text.h"
#include "menus.h"

/*Prototipos de funciones utilizadas*/
void Counting(void);
const char* GetDateStringFromRTC (void);
const char* GetTimeStringFromRTC (void);
void SetTimeStringtoRTC (char* str);
void SetDateStringtoRTC (char* str);
void TareaRELOJ (void *pvParameters);
void InitRTCtoDefault (void);
void ForceRTCtoSpecific (void);
void TareaFechaHora(void *pvParameters);
void BlinkFechaHora(Eventos EventId, char param2);
void SeteaDataHora (Eventos EventId, char param2);
void SeteaDataFecha (Eventos EventId, char param2);
