/*
 * menus.c
 *
 *  Created on: /08/2017
 *      Author: Gerardo Zoffoli
 */

#define CANTIDAD_MENUS 6
#include "AD7793.h"
#include "menus.h"
#include "Teclado.h"
#include "PANTALLA.h"
#include "RELOJ.h"
#include "TARJETA_SD.h"

/*********************** Manejador de los menus ***********************/
static MenuHandler Principal, SubMenu, Sub_SubMenu;
static MenuHandler* Current;
static LCDtext* ptrDisplayLCD;
/**********************************************************************/

//Variables externas
extern xQueueHandle ColaTECLADO;
extern GPIO ledStick;

/****************** DEFINO LENGTH DE MENU/SUBMENU *********************************/

#define PADRE_LEN       	5
#define SUB_MED_LEN	    	2
#define SUB_AJUSTE_LEN		4
#define SUB_FECHAHORA_LEN 	2
#define SUB_ETHERNET_LEN  	3
#define SUB_CALIB_LEN	  	3

#define UNO					0
#define DOS					1
#define TRES				2
#define CUATRO				3
#define CINCO				4

/****************** DEFINO/CREO LOS VECTORES DEL MENU *****************************/

MenuItem ArrayPadres         [PADRE_LEN         ];
MenuItem SubMenuMedicion     [SUB_MED_LEN       ];
MenuItem SubMenuAjuste		 [SUB_AJUSTE_LEN    ];
MenuItem SubMenuCalibracion  [SUB_CALIB_LEN    ];
MenuItem SubMenu_2_FechaHora [SUB_FECHAHORA_LEN ];
MenuItem SubMenu_2_Ethernet  [SUB_ETHERNET_LEN  ];

/****************** DEFINO VARIABLES MENUS ****************************************/

char Titulos_1[5][16]={
		"MEDICION",
		"AJUSTE",
		"CALIBRACION",
		"FECHA/HORA",
		"TOGGLELED"}; // La función ToggleLed esta como ejemplo para probar de enviar una función como argumento

char Titulos_Medicion[2][16]={
		"DATO DE SD",
		"VALOR ACTUAL"};

char Titulos_Calibracion[3][16]={
		"pH Refer: 4.000",
		"pH Refer: 7.000",
		"pH Refer: 10.000"};

char Titulos_Ajuste[4][16]={
		"DEFAULT",
		"FECHA / HORA",
		"ETHERNET",
		"SD"};

char Titulos_FechaHora[4][16]={
		"HH:MM:SS",
		"DD:MM:AA"};

char Titulos_Ethernet[4][16]={
		"IP",
		"DEFAULT GATEWAY",
		"NETMASK"};

/**********************************************************************************/


void CalcularIndice(Util Dir )
{
	if(Dir == ATRAS)
	{
		Current->Anterior = Current->Indice;
		if(Current->Indice == 0)
		{
			Current->Indice = Current->Elementos-1 ;
			return;
		}
		else
		{
			Current->Indice--;
			return;
		}
	}

	if(Dir == ADELANTA)
	{
		Current->Anterior = Current->Indice;
		if(Current->Indice == (Current->Elementos-1) )
		{
			Current->Indice = 0;
		}
		else
		{
			Current->Indice = Current->Indice+1;
		}
	}
	return;

}//Final CalcukarIndice()

void Subir_Menu(void)
{
	CalcularIndice(ATRAS);
}// fin funcion Subir_Menu()

void Bajar_Menu(void)
{
    CalcularIndice(ADELANTA);
}// fin funcion Bajar_Menu()

void Imprimir_menu(void)
{
	LCDtextClear(ptrDisplayLCD);
	LCDtextGotoXY(ptrDisplayLCD, 0, 0);
	LCDtextPuts(ptrDisplayLCD, Current->Menus[Current->Indice].Titulo );
	PrintReturnMsg();
}//fin funcion

void PrintReturnMsg(void){
	if(Current->DondeEstoy != PADRE)
	{
		LCDtextGotoXY(ptrDisplayLCD, 6, 1);
		LCDtextPuts(ptrDisplayLCD, "Volver: A");
	}
}

MenuHandler* CambioMenu(MenuHandler* Current)
{
	Current->Elementos = 0;
	while( Current->Menus[Current->Elementos].esUltimo == false )
	{
		Current->Elementos++;
	}
	if(Current->Elementos == 0)
		return(NULL);
	Current->Elementos++;
	Current->Indice = 0;
	Current->Anterior = 1;
	return(Current);
}

void SelectMenu(Eventos ID_Event)
{
	// Si el menu tiene asociado alguna accion que espera un tipo de evento la invoco en este punto.
	if(Current->Menus[Current->Indice].hEvento_Accion != NULL)
	{
		Current->Menus[Current->Indice].hEvento_Accion(ID_Event, 0);
	}
	//El item del menu donde me encuentro tiene un submenu, entonces lo muevo el current.

	if(Current->Menus[Current->Indice].Hijos != NULL)
	{
		switch(Current->DondeEstoy)
		{
			case  PADRE:
				SubMenu.Menus =  Current->Menus[Current->Indice].Hijos ;
				Current = CambioMenu(&SubMenu);
				break;
			case  SUBMENU_1:
				Sub_SubMenu.Menus =  Current->Menus[Current->Indice].Hijos ;
				Current = CambioMenu(&Sub_SubMenu);
				break;
			case  SUBMENU_2:
				break;
		}
	}
}// Fin SelectMenu();

void VolverMenuAnterior(void)
{   //Principal, SubMenu, Sub_SubMenu
	switch(Current->DondeEstoy)
	{
		case PADRE:
			// no hay menus ateriores.
			break;
		case SUBMENU_1:
			Current = &Principal;
			break;
		case SUBMENU_2:
			Current = &SubMenu;
			break;
	}
}//Final VolverMenuAnterior()

void EventDispatchMenu(Eventos ID_Event, char param2)  //Punto de entrada para procesar todos los eventos que afectan a los menus.
{

		switch(ID_Event)
		{
			case SUBIR:
				//Subir_Menu();
				CalcularIndice(ATRAS);
				break;
			case BAJAR:
				//Bajar_Menu();
				CalcularIndice(ADELANTA);
				break;
			case SELECCION:
				SelectMenu(SELECCION);
				break;
			case VOLVER:
				VolverMenuAnterior();
				break;
			case SD:
				SelectMenu(SD);
			case CALIBRAR:
				SelectMenu(CALIBRAR);
			default:
				break;
		}

		if(ID_Event!=SD && ID_Event!=CALIBRAR)
			Imprimir_menu();

}//Final EventDispatchMenu


/************************** inicializaion y creacion de los menus *****************************/
/*
 * Inicilaiza los manejadores.
 * luego una vez inicializados hay que crear los items y sub items de cada menu.
 *
 */
void InitMenus( LCDtext* HandleDisplay  )
{
	///////
	ptrDisplayLCD = HandleDisplay;
	Current = &Principal;
	memset(&Principal,   0x00, sizeof(Principal) );
	memset(&SubMenu,     0x00, sizeof(SubMenu)   );
	memset(&Sub_SubMenu, 0x00, sizeof(Sub_SubMenu));
	///////
	Principal.DondeEstoy = PADRE;
	SubMenu.DondeEstoy = SUBMENU_1;
	Sub_SubMenu.DondeEstoy = SUBMENU_2;
}

void CrearMenuPadres( MenuItem *Array, uint16_t Leng    )
{
	Principal.Menus = Array;
	memset(Principal.Menus,0x00, Leng );     // borro todo para estar seguro.
	Principal.Elementos = Leng;
	Principal.Menus[Leng-1].esUltimo = true; // Marco el ultimo
}

EstadoResultado CrearSubMenu_1( uint16_t PadreIdx, MenuItem Array[],uint16_t Leng )
{
	Principal.Menus[PadreIdx].Hijos = Array;
	SubMenu.Menus = Array;
	memset(SubMenu.Menus,0x00, Leng );     // borro to_do para estar seguro.
	SubMenu.Menus[Leng-1].esUltimo = true; // Marco el ulrimo
	return( CREADO );
}

EstadoResultado CrearSubMenu_2( uint16_t PadreIdx, uint16_t HijoIdx, MenuItem Array[],uint16_t Leng  )
{
	// todo antes de hacer nada fijarce si esta utilizado y si no es un elemento mayor de lo permitido
	Principal.Menus[PadreIdx].Hijos[HijoIdx].Hijos = Array;
	Sub_SubMenu.Menus = Array;                 //lo utilizo para simplificar
	memset(Sub_SubMenu.Menus,0x00, Leng );     // borro to_do para estar seguro.
	Sub_SubMenu.Menus[Leng-1].esUltimo = true; // Marco el ulrimo
	return( CREADO );
}

EstadoResultado PrimerItemLibre(MenuHandler* Current, uint16_t* PosLibre)
{
	uint16_t idx = 0;
	bool Seguir = true;
	do{
		if( strcmp(Current->Menus[idx].Titulo, "") == 0 )
		{
			*PosLibre = idx;
			Seguir = false;
		}
		else
		{
			if(Current->Menus[idx].esUltimo )
				return(MEMORIA_LLENA);
		}
		idx++;
	}while(Seguir);
	return(SIN_ERROR);
}

EstadoResultado AgregarItemPadres( char* Nombre, uint8_t NombreLeng, void (*FuncionEvento)(Eventos EventId, char param2) )
{
	uint16_t Pos;
	EstadoResultado HayError;
	HayError = PrimerItemLibre(&Principal, &Pos);
	if( HayError != SIN_ERROR )
		return(HayError);
	return( AgregarItem(&Principal.Menus[Pos], Nombre, NombreLeng, FuncionEvento ) );
}

EstadoResultado AgregarItemSub_1(uint16_t PadreIdx, char* Nombre, uint8_t NombreLeng, void (*func)(Eventos, char) )
{
	uint16_t Pos;
	EstadoResultado HayError;
	SubMenu.Menus = Principal.Menus[PadreIdx].Hijos;
	HayError = PrimerItemLibre(&SubMenu, &Pos);
	if( HayError != SIN_ERROR )
		return(HayError);
	return( AgregarItem(&SubMenu.Menus[Pos], Nombre, NombreLeng, func ) );
}

EstadoResultado AgregarItemSub_2(uint16_t PadreIdx, uint16_t HijoIdx, char* Nombre, uint8_t NombreLeng, void (*func)(Eventos, char) )
{
	uint16_t Pos;
	EstadoResultado HayError;
	SubMenu.Menus = Principal.Menus[PadreIdx].Hijos;
	Sub_SubMenu.Menus = SubMenu.Menus[HijoIdx].Hijos;
	HayError = PrimerItemLibre(&Sub_SubMenu, &Pos);
	if( HayError != SIN_ERROR )
		return(HayError);
	return( AgregarItem(&Sub_SubMenu.Menus[Pos], Nombre, NombreLeng, func ) );
}

EstadoResultado AgregarItem(MenuItem* Current, char* Nombre, uint8_t NombreLeng, void (*func)(Eventos, char) )
{
	strcpy(Current->Titulo, Nombre);
	Current->hEvento_Accion = func;
	return( CREADO );
}



void CreardorMenus(void)
{
	/*
	 * Iniciliza los manejadores.
	 * luego una vez inicializados hay que crear los items y sub items de cada menu.
	 */
	InitMenus(&DisplayLCD);

		CrearMenuPadres( ArrayPadres, PADRE_LEN);
		AgregarItemPadres( Titulos_1[UNO], strlen(Titulos_1[UNO]), NULL );
		AgregarItemPadres( Titulos_1[DOS], strlen(Titulos_1[DOS]), NULL );
		AgregarItemPadres( Titulos_1[TRES], strlen(Titulos_1[TRES]), NULL );
		AgregarItemPadres( Titulos_1[CUATRO], strlen(Titulos_1[CUATRO]), BlinkFechaHora );
		AgregarItemPadres( Titulos_1[CINCO], strlen(Titulos_1[CINCO]), NULL);

		CrearSubMenu_1(UNO, SubMenuMedicion, SUB_MED_LEN );
		AgregarItemSub_1(UNO, Titulos_Medicion[UNO], strlen(Titulos_Medicion[UNO]), NULL );
		AgregarItemSub_1(UNO, Titulos_Medicion[DOS], strlen(Titulos_Medicion[DOS]), medicion_valor_actual );

		CrearSubMenu_1( DOS, SubMenuAjuste, SUB_AJUSTE_LEN );
		AgregarItemSub_1(DOS, Titulos_Ajuste [UNO], strlen(Titulos_Ajuste[UNO]), SystemToDefault );
		AgregarItemSub_1(DOS, Titulos_Ajuste [DOS], strlen(Titulos_Ajuste[DOS]), NULL );
		AgregarItemSub_1(DOS, Titulos_Ajuste [TRES], strlen(Titulos_Ajuste[TRES]), NULL );
		AgregarItemSub_1(DOS, Titulos_Ajuste [CUATRO], strlen(Titulos_Ajuste[CUATRO]), GetFreeSD );

		CrearSubMenu_1(TRES, SubMenuCalibracion, SUB_CALIB_LEN );
		AgregarItemSub_1(TRES, Titulos_Calibracion[UNO], strlen(Titulos_Calibracion[UNO]), calibracion_ph4 );
		AgregarItemSub_1(TRES, Titulos_Calibracion[DOS], strlen(Titulos_Calibracion[DOS]), calibracion_ph7 );
		AgregarItemSub_1(TRES, Titulos_Calibracion[TRES], strlen(Titulos_Calibracion[TRES]), calibracion_ph10 );

		CrearSubMenu_2( DOS, DOS, SubMenu_2_FechaHora, SUB_FECHAHORA_LEN );
		AgregarItemSub_2(DOS, DOS, Titulos_FechaHora [UNO], strlen(Titulos_FechaHora[UNO]), SeteaDataHora);
		AgregarItemSub_2(DOS, DOS, Titulos_FechaHora [DOS], strlen(Titulos_FechaHora[DOS]), SeteaDataFecha);

		CrearSubMenu_2( DOS, TRES, SubMenu_2_Ethernet, SUB_ETHERNET_LEN );
		AgregarItemSub_2(DOS, TRES, Titulos_Ethernet [UNO], strlen(Titulos_Ethernet[UNO]), NULL );
		AgregarItemSub_2(DOS, TRES, Titulos_Ethernet [DOS], strlen(Titulos_Ethernet[DOS]), NULL );
		AgregarItemSub_2(DOS, TRES, Titulos_Ethernet [TRES], strlen(Titulos_Ethernet[TRES]), NULL );

}



void TareaMenu(void *pvParameters)
{
	unsigned int tecla;

	// Solo al inicio
	LCDtextPuts(&DisplayLCD, "INICIANDO");
	vTaskDelay(500);
	LCDtextGotoXY(&DisplayLCD, 0, 0);
	LCDtextPuts(&DisplayLCD, "INICIANDO.");
	vTaskDelay(500);
	LCDtextGotoXY(&DisplayLCD, 0, 0);
	LCDtextPuts(&DisplayLCD, "INICIANDO..");
	vTaskDelay(500);
	LCDtextGotoXY(&DisplayLCD, 0, 0);
	LCDtextPuts(&DisplayLCD, "INICIANDO...");
	vTaskDelay(2000);
	LCDtextClear(&DisplayLCD);
	LCDtextPuts(&DisplayLCD, "pHmetro UTN 2015");
	LCDtextGotoXY(&DisplayLCD, 0, 1);
	LCDtextPuts(&DisplayLCD, "FW:2.8 HW:1.1 ");
	LCDtextGotoXY(&DisplayLCD, 16, 1);

	while(1)
	{
		if( xQueueReceive( ColaTECLADO, &tecla, portMAX_DELAY ) == pdTRUE )
		{
			//Acá empieza el código menú
			//Para empezar, se debe presiona las teclas UP o DOWN (modificadas en la declaración)
			//para navegar en el menú, a su vez se mostrará las opciones en el LCD, luego reconoce las teclas enter

			switch(tecla)
			{
				case TECLA_UP:
					EventDispatchMenu( SUBIR, 0);
					break;
				case TECLA_DOWN:
					EventDispatchMenu(BAJAR, 0);
					break;
				case TECLA_F1:
					EventDispatchMenu(VOLVER, 0);
					break;
				case TECLA_F2:
					EventDispatchMenu(SELECCION, 0);
					break;
				case TECLA_AST:
					EventDispatchMenu(SD, 0);
					break;
				case TECLA_NUM:
					EventDispatchMenu(CALIBRAR, 0);
					break;
				default :
					break;

			}

		}
	}
}




void TareaAutoDispatcher(void *pvParameters)
{
	portTickType xMeDesperte;

	while(1)
	{

		if(Current->Menus[Current->Indice].hEvento_Accion != 0)
			Current->Menus[Current->Indice].hEvento_Accion(EJECUTAR, 0);

		vTaskDelayUntil(&xMeDesperte,1000);
	}
}


//==============================================================================================
//		-		Proposito: Vover a Default el equipo
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================

void SystemToDefault (Eventos EventID, char param2)
{

	if(EventID==SELECCION)
	{	LCDtextClear(&DisplayLCD);
		LCDtextPuts(&DisplayLCD, "Inicializando");
		vTaskDelay(500);
		LCDtextGotoXY(&DisplayLCD, 0, 0);
		LCDtextPuts(&DisplayLCD, "Inicializando.");
		vTaskDelay(500);
		LCDtextGotoXY(&DisplayLCD, 0, 0);
		LCDtextPuts(&DisplayLCD, "Inicializando..");
		vTaskDelay(500);
		LCDtextGotoXY(&DisplayLCD, 0, 0);
		LCDtextPuts(&DisplayLCD, "Inicializando...");

		InitRTCtoDefault();
		AD7793_Reset();
		AD7793_Init();

		vTaskDelay(500);
		LCDtextClear(&DisplayLCD);
		LCDtextPuts(&DisplayLCD, "OK!!!");
		vTaskDelay(500);
	}

}


/****************** FUNCIÓN A BORRAR ***************************************************/

void ToggeLed(void)
{

		if(isActivo(&ledStick))
		{
			Pasivar(&ledStick);
		}
		else
		{
			Activar(&ledStick);
		}

}


/*************************************************************************************/
