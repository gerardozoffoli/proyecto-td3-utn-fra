/*
 * RELOJ.c
 *
 *  Created on: /08/2017
 *      Author: Gerardo
 */

//Incluyo las definiciones de regitros, pines y perifericos segun el micro que use
#include "TARJETA_SD.h"
#include "RELOJ.h"
#include "PANTALLA.h"

//CAPI
#include "ColaCircular.h"
#include "GPIO.h"
#include "BUS.h"
#include "UART.h"
#include "AIN.h"
#include "SSP.h"

//Libreria estandar C
#include <stdio.h>	//Uso sprintf
#include <string.h>	//Uso strlen

//Librería para uso de SD.
#include "ff.h"
#include "CAPI_diskio.h"

//Cola de mensajes
xQueueHandle ColaSD = NULL;

//==============================================================================================
//		-		Proposito: Guarda Log de medicion en el ROOT de la SD
//		-		Parametros: int valorPH, int valorTemp
//		-		Retorna: void
//==============================================================================================

void TareaRegistrar(void *pvParameters)
{
	FIL 	fp;
	FILINFO	fs;
	FATFS 	Fatfs[1];
	FRESULT fr;
	int 	bw, len,i;
	char 	timestamp[20];
	char 	msg[20];
	char 	datos[50];
	char 	aux;
	char    fecha[8];
	char    hora[6];
	SDdata  d;
	RTCDate mifecha;


	while(1)
	{

		if((xQueueReceive(ColaSD, &d, portMAX_DELAY))==pdTRUE){

		RTC_GetFullTime(LPC_RTC, (RTC_TIME_Type*) &mifecha);

		fecha[0] = (mifecha.year/1000)+48;
		aux = mifecha.year%1000;
		fecha[1] = (aux/100)+48 ;
		aux = mifecha.year%100;
		fecha[2] = (aux/10)+48;
		fecha[3] = (aux%10)+48;
		fecha[4] = (mifecha.month/10)+48;
		fecha[5] = (mifecha.month%10)+48;
		fecha[6] = (mifecha.dayofmonth/10)+48;
		fecha[7] = (mifecha.dayofmonth%10)+48;
		fecha[8] = '\0';

		hora[0] = (mifecha.hours/10)+48;
		hora[1] = (mifecha.hours%10)+48;
		hora[2] = (mifecha.minutes/10)+48;
		hora[3] = (mifecha.minutes%10)+48;
		hora[4] = (mifecha.seconds/10)+48;
		hora[5] = (mifecha.seconds%10)+48;
		hora[6] = '\0';

		timestamp[0] = '/';

		for(i=1; i<20; i++)
			timestamp[i] = '\0';

		strcat(timestamp, hora);
		strcat(timestamp, ".log");
		strcpy(msg, timestamp);

		sprintf(datos,"\nTIME: %s ----> PH: %.3f | Temp: %.2f\r\n",hora,d.pH,d.temp);
		len = strlen(datos);

		taskDISABLE_INTERRUPTS();
			f_mount(0, &Fatfs[0]);
			fr = f_open(&fp,(const XCHAR*)msg,FA_OPEN_ALWAYS | FA_WRITE);

			if(fr==FR_OK){
				f_stat(msg,&fs);
				f_lseek(&fp,fs.fsize);
				f_sync(&fp);
				f_write(&fp,(const void*)datos,len,(UINT*)&bw);
				f_sync(&fp);

				LCDtextClear(&DisplayLCD);
				LCDtextPuts(&DisplayLCD, "GUARDANDO...");
				vTaskDelay(1000);

				}
			else{
				LCDtextClear(&DisplayLCD);
				LCDtextPuts(&DisplayLCD, "ERROR!!!...");
				vTaskDelay(1000);
			}

			f_close(&fp);
			f_mount(0, NULL);
		taskENABLE_INTERRUPTS();
		}
	}

}



//==============================================================================================
//		-		Proposito: Verificar si hay una tarjeta SD insertada en el equipo
//		-		Parametros: void
//		-		Retorna: bool
//==============================================================================================
int CheckForSD (void)
{
	DSTATUS s;

	s = disk_initialize(0);

    if(s==FR_OK)
    	return true;
    else
    	return true;

}



//==============================================================================================
//		-		Proposito: Mostrar espacio usado y disponible en la tarjeta SD
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================
void GetFreeSD (Eventos IdEvent, char param2)
{
	char msg[16];
	FATFS *fs;
	FATFS 	Fatfs;
	DWORD fre_clust, fre_sect, tot_sect;
	FRESULT fr;


	if(IdEvent==SELECCION)
	{
		f_mount(0, &Fatfs);
		fr = f_getfree("0:", &fre_clust, &fs);
		f_mount(0, NULL);

		fre_sect = fre_clust * fs->csize;

		/* Print the free space (assuming 512 bytes/sector) */
		sprintf(msg,"%luKB Libres", fre_sect / 2);

		if(fr==FR_OK){
			LCDtextClear(&DisplayLCD);
			LCDtextPuts(&DisplayLCD, "Actualmente:");
			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, msg);
			vTaskDelay(2000);
		}
		else
		{
			LCDtextClear(&DisplayLCD);
			LCDtextPuts(&DisplayLCD, "SD no insertada ");
			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, "en el slot!!!");
			vTaskDelay(2000);
		}
	}
	else
	{

	}


}
