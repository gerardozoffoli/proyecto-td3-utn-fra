/***************************************************************************//**
 *   @file   AD7793.c
 *   @brief  Implementation of AD7793 Driver.
 *   @author Bancisor MIhai
********************************************************************************
 * Copyright 2012(c) Analog Devices, Inc.
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *  - Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  - Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in
 *    the documentation and/or other materials provided with the
 *    distribution.
 *  - Neither the name of Analog Devices, Inc. nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *  - The use of this software may or may not infringe the patent rights
 *    of one or more patent holders.  This license does not release you
 *    from the requirement that you obtain separate licenses from these
 *    patent holders to use this software.
 *  - Use of the software either in source or binary form, must be run
 *    on or directly connected to an Analog Devices Inc. component.
 *
 * THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR
 * IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT,
 * MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 * IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
 * LIMITED TO, INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
********************************************************************************
 *   SVN Revision: 500
*******************************************************************************/

/******************************************************************************/
/* Include Files                                                              */
/******************************************************************************/
#include "AD7793.h"				// AD7793 definitions.
#include "Communication.h"		// Communication definitions.
#include "CAPI_capa_SPI.h"
#include "GPIO.h"
#include "lpc_176X_PinNames.h"
#include "menus.h"
#include "PANTALLA.h"
#include "TARJETA_SD.h"

//Variables externas
extern xQueueHandle ColaSD;

//Variables globales
float span_pH = 0.9463;
float ZERO_pH = -0.3157;

/***************************************************************************//**
 * @brief Initializes the AD7793 and checks if the device is present.
 *
 * @return status - Result of the initialization procedure.
 *                  Example: 1 - if initialization was successful (ID is 0x0B).
 *                           0 - if initialization was unsuccessful.
*******************************************************************************/
unsigned char AD7793_Init(void)
{ 
	unsigned char status = 0x1;
    
	CAPI_SPI_Init();
	AD7793_Reset();
    CAPI_SPI_SelectAD();

    if((AD7793_GetRegisterValue(AD7793_REG_ID, 1, 1) & 0x0F) != AD7793_ID)
	{
		status = 0x0;
	}

    CAPI_SPI_SendByte (0x28); // Le digo que voy a escribir en el registo "IO Register" de 1 byte
    CAPI_SPI_SendByte (0x02); // Le digo que use el Generador de corriente 2 en 210uA

	CAPI_SPI_DeSelectAD();

	return(status);
}

/***************************************************************************//**
 * @brief Sends 32 consecutive 1's on SPI in order to reset the part.
 *
 * @return  None.    
*******************************************************************************/
void AD7793_Reset(void)
{
    CAPI_SPI_SelectAD();

    CAPI_SPI_SendByte (0xFF);
    CAPI_SPI_SendByte (0xFF);
    CAPI_SPI_SendByte (0xFF);
    CAPI_SPI_SendByte (0xFF);

	CAPI_SPI_DeSelectAD();
}

/***************************************************************************//**
 * @brief lee el canal 1 del AD
 *
 * @return data - Cuenta del canal 2.
*******************************************************************************/
unsigned long AD7793_leer_canal_1(unsigned char sampleNumber)
{
	unsigned long canal_1 = 0x00;

	CAPI_SPI_SelectAD(); // selecciono la comunicacion con el AD

    CAPI_SPI_SendByte (0x10); //le digo que voy a escribir en el registro "CONFIGURATION REGISTER" que es de 2 byte
    CAPI_SPI_SendByte (0x00); //escribo en el regsitro para decirle al AD que seleccione el canal 1
    CAPI_SPI_SendByte (0x10); //escribo en el regsitro para decirle al AD que seleccione el canal 1
    canal_1 = AD7793_ContinuousReadAvg(sampleNumber);

	CAPI_SPI_DeSelectAD();// deselecciono la comunicacion con el AD

	return (canal_1);
}


/***************************************************************************//**
 * @brief lee el canal 2 del AD
 *
 * @return data - Cuenta del canal 2.
*******************************************************************************/
unsigned long AD7793_leer_canal_2(unsigned char sampleNumber)
{
	unsigned long canal_2 = 0x00;

	CAPI_SPI_SelectAD();

    CAPI_SPI_SendByte (0x10); //le digo que voy a escribir en el registro "CONFIGURATION REGISTER" que es de 2 byte
    CAPI_SPI_SendByte (0x00); //escribo en el regsitro para decirle al AD que seleccione el canal 2
    CAPI_SPI_SendByte (0x11); //escribo en el regsitro para decirle al AD que seleccione el canal 2
    canal_2 = AD7793_ContinuousReadAvg(sampleNumber);

	CAPI_SPI_DeSelectAD();

	return (canal_2);
}


/***************************************************************************//**
 * @brief Reads the value of the selected register
 *
 * @param regAddress - The address of the register to read.
 * @param size - The size of the register to read.
 *
 * @return data - The value of the selected register register.
*******************************************************************************/
unsigned long AD7793_GetRegisterValue(unsigned char regAddress, 
                                      unsigned char size,
                                      unsigned char modifyCS)
{
	unsigned char data[5]      = {0x00, 0x00, 0x00, 0x00, 0x00};
	unsigned long receivedData = 0x00;
    unsigned char i            = 0x00; 
    
	data[0] = 0x01 * modifyCS;
	data[1] = AD7793_COMM_READ |  AD7793_COMM_ADDR(regAddress); 
	SPI_Read(data,(1 + size));
	for(i = 1;i < size + 1;i ++)
    {
        receivedData = (receivedData << 8) + data[i];
    }
    
    return (receivedData);
}





/***************************************************************************//**
 * @brief Writes the value to the register
 *
 * @param -  regAddress - The address of the register to write to.
 * @param -  regValue - The value to write to the register.
 * @param -  size - The size of the register to write.
 *
 * @return  None.    
*******************************************************************************/
void AD7793_SetRegisterValue(unsigned char regAddress,
                             unsigned long regValue, 
                             unsigned char size,
                             unsigned char modifyCS)
{
	unsigned char data[5]      = {0x00, 0x00, 0x00, 0x00, 0x00};	
	unsigned char* dataPointer = (unsigned char*)&regValue;
    unsigned char bytesNr      = size + 1;
    
    data[0] = 0x01 * modifyCS;
    data[1] = AD7793_COMM_WRITE |  AD7793_COMM_ADDR(regAddress);
    while(bytesNr > 1)
    {
        data[bytesNr] = *dataPointer;
        dataPointer ++;
        bytesNr --;
    }	    
	SPI_Write(data,(1 + size));
}
/***************************************************************************//**
 * @brief  Waits for RDY pin to go low.
 *
 * @return None.
*******************************************************************************/
void AD7793_WaitRdyGoLow(void)
{
	CAPI_SPI_SendByte (0x40);

	while((CAPI_SPI_RecvByte () & AD7793_STAT_RDY)==AD7793_STAT_RDY)
		CAPI_SPI_SendByte (0x40);
}

/***************************************************************************//**
 * @brief Sets the operating mode of AD7793.
 *
 * @param mode - Mode of operation.
 *
 * @return  None.    
*******************************************************************************/
void AD7793_SetMode(unsigned long mode)
{
    unsigned long command;
    
    command = AD7793_GetRegisterValue(AD7793_REG_MODE,
                                      2,
                                      1); // CS is modified by SPI read/write functions.
    command &= ~AD7793_MODE_SEL(0xFF);
    command |= AD7793_MODE_SEL(mode);
    AD7793_SetRegisterValue(
            AD7793_REG_MODE,
            command,
            2, 
            1); // CS is modified by SPI read/write functions.
}
/***************************************************************************//**
 * @brief Selects the channel of AD7793.
 *
 * @param  channel - ADC channel selection.
 *
 * @return  None.    
*******************************************************************************/
void AD7793_SetChannel(unsigned long channel)
{
    unsigned long command;
    
    command = AD7793_GetRegisterValue(AD7793_REG_CONF,
                                      2,
                                      1); // CS is modified by SPI read/write functions.
    command &= ~AD7793_CONF_CHAN(0xFF);
    command |= AD7793_CONF_CHAN(channel);
    AD7793_SetRegisterValue(
            AD7793_REG_CONF,
            command,
            2,
            1); // CS is modified by SPI read/write functions.
}

/***************************************************************************//**
 * @brief  Sets the gain of the In-Amp.
 *
 * @param  gain - Gain.
 *
 * @return  None.    
*******************************************************************************/
void AD7793_SetGain(unsigned long gain)
{
    unsigned long command=0x00;

    command = AD7793_GetRegisterValue(AD7793_REG_CONF,2,1); // CS is modified by SPI read/write functions.
    command &= ~AD7793_CONF_GAIN(0xFF);
    command |= AD7793_CONF_GAIN(gain);
    AD7793_SetRegisterValue(AD7793_REG_CONF,command,2,1); // CS is modified by SPI read/write functions.

}
/***************************************************************************//**
 * @brief Sets the reference source for the ADC.
 *
 * @param type - Type of the reference.
 *               Example: AD7793_REFSEL_EXT	- External Reference Selected
 *                        AD7793_REFSEL_INT	- Internal Reference Selected.
 *
 * @return None.    
*******************************************************************************/
void AD7793_SetIntReference(unsigned char type)
{
    unsigned long command = 0;
    
    command = AD7793_GetRegisterValue(AD7793_REG_CONF,
                                      2,
                                      1); // CS is modified by SPI read/write functions.
    command &= ~AD7793_CONF_REFSEL(AD7793_REFSEL_INT);
    command |= AD7793_CONF_REFSEL(type);
    AD7793_SetRegisterValue(AD7793_REG_CONF,
							command,
							2,
                            1); // CS is modified by SPI read/write functions.
}

/***************************************************************************//**
 * @brief Performs the given calibration to the specified channel.
 *
 * @param mode - Calibration type.
 * @param channel - Channel to be calibrated.
 *
 * @return none.
*******************************************************************************/
void AD7793_Calibrate(unsigned char mode, unsigned char channel)
{
    unsigned short oldRegValue = 0x0;
    unsigned short newRegValue = 0x0;
    
    AD7793_SetChannel(channel);
    oldRegValue &= AD7793_GetRegisterValue(AD7793_REG_MODE, 2, 1); // CS is modified by SPI read/write functions.
    oldRegValue &= ~AD7793_MODE_SEL(0x7);
    newRegValue = oldRegValue | AD7793_MODE_SEL(mode);
    ADI_PART_CS_LOW; 
    AD7793_SetRegisterValue(AD7793_REG_MODE, newRegValue, 2, 0); // CS is not modified by SPI read/write functions.
    AD7793_WaitRdyGoLow();
    ADI_PART_CS_HIGH;
    
}

/***************************************************************************//**
 * @brief Returns the result of a single conversion.
 *
 * @return regData - Result of a single analog-to-digital conversion.
*******************************************************************************/
unsigned long AD7793_SingleConversion(void)
{
    unsigned long command = 0x0;
    unsigned long regData = 0x0;
    
    command  = AD7793_MODE_SEL(AD7793_MODE_SINGLE);
    ADI_PART_CS_LOW;
    AD7793_SetRegisterValue(AD7793_REG_MODE, 
                            command,
                            2,
                            0);// CS is not modified by SPI read/write functions.
    AD7793_WaitRdyGoLow();
    regData = AD7793_GetRegisterValue(AD7793_REG_DATA, 3, 0); // CS is not modified by SPI read/write functions.
    ADI_PART_CS_HIGH;

    return(regData);
}

/***************************************************************************//**
 * @brief Returns the average of several conversion results.
 *
 * @return samplesAverage - The average of the conversion results.
*******************************************************************************/
unsigned long AD7793_ContinuousReadAvg(unsigned char sampleNumber)
{
    unsigned long samplesAverage = 0x0;
//    unsigned long command        = 0x0;
    unsigned char count          = 0x0;

//    command = AD7793_MODE_SEL(AD7793_MODE_CONT);
/*    AD7793_SetRegisterValue(AD7793_REG_MODE,
                            command, 
                            2,
                            0);// CS is not modified by SPI read/write functions.*/
    for(count = 0;count < sampleNumber;count ++)
    {
        AD7793_WaitRdyGoLow(); // espera tener una convercion completa
        samplesAverage += AD7793_GetRegisterValue(AD7793_REG_DATA, 3, 0);  // CS is not modified by SPI read/write functions.
    }
    samplesAverage = samplesAverage / sampleNumber;
    
    return(samplesAverage);
}

/***************************************************************************//**
 * @brief Returns valor de temperatura
 * @return samplesAverage - The average of the conversion results.
*******************************************************************************/
float temperatura(void)
{
	float temp = 0x00;
	float rtd = 0x00;
	float zero = -1.270;
	float span = 1.0028;
	unsigned long code = 0x00;


	code=AD7793_leer_canal_2(10); // devuelve la cuenta de un promedio 10 mediciones
	rtd = ((((float)code/(float)8388608)-1)*1.05)/0.000214; // Paso la cuenta a V y luego lo divido por la I del generador para obtener la resistencia de la PT1000
	temp = (((rtd/(1000))-1)/(0.00385))*(span)+zero; // calculo la temperatura en funcion de la Resistencia de la PT1000 obtenida y corrigo el ZERO y el SPAN (segun la calibracion realizada)

	return (temp);
}
/***************************************************************************//**
 * @brief Returns valor de .
 *
 * @return samplesAverage - The average of the conversion results.
*******************************************************************************/
float pH(float temp)
{
	float pH = 0x00;
	unsigned long code = 0x00;

	code=AD7793_leer_canal_1(10);// devuelve la cuenta de un promedio 10 mediciones
	pH = (((7-((((float)code/(float)8388608)-1)*1.05)/(0.05916+0.0001984*(temp-25))))+ZERO_pH)/span_pH;

	return(pH);
}



void medicion_valor_actual (Eventos EventId, char param2)
{
	char Str_pH[16] = "pH:  ";
	char Str_temp[16] = "Temp: ";
	char aux[16];
	float temp, valor_pH;
	SDdata d;

	temp = temperatura();
	valor_pH = pH(temp);

	//temp = 22.1;
	//valor_pH = 4.003;

	d.pH = valor_pH;
	d.temp = temp;

	sprintf(aux,"%.1f",temp);
	strcat(Str_temp, aux);
	strcat(Str_temp, " C   ");

	if((valor_pH<0)||(valor_pH>14))
	{
		strcpy(aux,"Over range");
		strcat(Str_pH, aux);
		strcat(Str_pH, " ");
	}
	else
	{
		sprintf(aux,"%.3f",valor_pH);
		strcat(Str_pH, aux);
		strcat(Str_pH, "    ");
	}

	if(EventId==SD){

		if(CheckForSD()==true)
		{
			xQueueSend(ColaSD, &d, 0);
			vTaskDelay(1000);
		}
		else
		{
			LCDtextClear(&DisplayLCD);
			LCDtextPuts(&DisplayLCD, "NO DISK!!!...");
			vTaskDelay(1000);
		}
	}
	else
	{
		LCDtextGotoXY(&DisplayLCD, 0, 0);
		LCDtextPuts(&DisplayLCD, Str_pH);
		LCDtextGotoXY(&DisplayLCD, 0, 1);
		LCDtextPuts(&DisplayLCD, Str_temp);
	}
}



void calibracion_ph4 (Eventos EventId, char param2)
{
	char Str_pH[16] = "pH:  ";
	char Str_temp[16] = "Temp: ";
	char aux[16];
	float temp, valor_pH;
	float valor_pH4, pH4, valor_pH7, pH7;


	temp = temperatura();
	valor_pH = pH(temp);

	//temp = 22.1;
	//valor_pH = 4.003;

	sprintf(aux,"%.1f",temp);
	strcat(Str_temp, aux);
	strcat(Str_temp, " C   ");

	if((valor_pH<0)||(valor_pH>14))
	{
		strcpy(aux,"Over range");
		strcat(Str_pH, aux);
		strcat(Str_pH, " ");
	}
	else
	{
		sprintf(aux,"%.3f",valor_pH);
		strcat(Str_pH, aux);
		strcat(Str_pH, "     ");
	}


		if(EventId==CALIBRAR){

			temp = temperatura();
			pH4 = temp*0,002+3,96;
			valor_pH4 = pH(temp);

			temp = temperatura();
			pH7 = temp*0,002+6,96;
			valor_pH7 = pH(temp);

			span_pH = (valor_pH7-valor_pH4)/(pH7-pH4);
			ZERO_pH = (span_pH*pH7)-valor_pH7;

			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, "SETEANDO...");
			vTaskDelay(1000);


		}
		else
		{
			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, Str_pH);
		}


}


void calibracion_ph7 (Eventos EventId, char param2)
{
	char Str_pH[16] = "pH:  ";
	char Str_temp[16] = "Temp: ";
	char aux[16];
	float temp, valor_pH;
	float valor_pH7, pH7, valor_pH10, pH10;

	temp = temperatura();
	valor_pH = pH(temp);

	//temp = 22.1;
	//valor_pH = 4.003;

	sprintf(aux,"%.1f",temp);
	strcat(Str_temp, aux);
	strcat(Str_temp, " C   ");

	if((valor_pH<0)||(valor_pH>14))
	{
		strcpy(aux,"Over range");
		strcat(Str_pH, aux);
		strcat(Str_pH, " ");
	}
	else
	{
		sprintf(aux,"%.3f",valor_pH);
		strcat(Str_pH, aux);
		strcat(Str_pH, "     ");
	}


		if(EventId==CALIBRAR){

			temp = temperatura();
			pH10 = temp*0,002+9,96;
			valor_pH10 = pH(temp);

			temp = temperatura();
			pH7 = temp*0,002+6,96;
			valor_pH7 = pH(temp);

			span_pH = (valor_pH10-valor_pH7)/(pH10-pH7);
			ZERO_pH = (span_pH*pH7)-valor_pH7;

			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, "SETEANDO...");
			vTaskDelay(1000);


		}
		else
		{
			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, Str_pH);
		}

}


void calibracion_ph10 (Eventos EventId, char param2)
{
	char Str_pH[16] = "pH:  ";
	char Str_temp[16] = "Temp: ";
	char aux[16];
	float temp, valor_pH;
	float valor_pH7, pH7, valor_pH10, pH10;

	temp = temperatura();
	valor_pH = pH(temp);

	//temp = 22.1;
	//valor_pH = 4.003;

	sprintf(aux,"%.1f",temp);
	strcat(Str_temp, aux);
	strcat(Str_temp, " C   ");

	if((valor_pH<0)||(valor_pH>14))
	{
		strcpy(aux,"Over range");
		strcat(Str_pH, aux);
		strcat(Str_pH, " ");
	}
	else
	{
		sprintf(aux,"%.3f",valor_pH);
		strcat(Str_pH, aux);
		strcat(Str_pH, "     ");
	}


		if(EventId==CALIBRAR){

			temp = temperatura();
			pH10 = temp*0,002+9,96;
			valor_pH10 = pH(temp);

			temp = temperatura();
			pH7 = temp*0,002+6,96;
			valor_pH7 = pH(temp);

			span_pH = (valor_pH10-valor_pH7)/(pH10-pH7);
			ZERO_pH = (span_pH*pH7)-valor_pH7;

			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, "SETEANDO...");
			vTaskDelay(1000);

		}
		else
		{
			LCDtextGotoXY(&DisplayLCD, 0, 1);
			LCDtextPuts(&DisplayLCD, Str_pH);
		}

}


/*

void AD7793_Calibrate_pH4(Eventos EventId, char param2)
{
	float temp, valor_pH4, pH4, valor_pH7, pH7, valor_pH7, pH7;
	char Str_pH[16] = "pH:  ";
	char Str_temp[16] = "Temp: ";
	char aux[16];
	float temp, valor_pH;

	temp = temperatura();
	pH4 = temp*0,002+3,96;

	valor_pH4 = pH(temp);

	temp = temperatura();
	pH7 = temp*0,002+6,96;
	valor_pH7 = pH(temp);

	span_pH = (valor_pH7-valor_pH4)/(pH7-pH4);
	ZERO_pH = (span_pH*pH7)-valor_pH7;
	sprintf(aux,"%.1f",temp);
	strcat(Str_temp, aux);
	strcat(Str_temp, " C   ");

	if((valor_pH<0)||(valor_pH>14))
	{
		strcpy(aux,"Over range");
		strcat(Str_pH, aux);
		strcat(Str_pH, " ");
	}
	else
	{
		sprintf(aux,"%.3f",valor_pH);
		strcat(Str_pH, aux);
		strcat(Str_pH, "    ");
	}

	LCDtextGotoXY(&DisplayLCD, 0, 0);
	LCDtextPuts(&DisplayLCD, Str_pH);

	LCDtextGotoXY(&DisplayLCD, 0, 1);
	LCDtextPuts(&DisplayLCD, Str_temp);




}

void AD7793_Calibrate_pH10(Eventos EventId, char param2)
{
	float temp, valor_pH7, pH7, valor_pH10, pH10;
	temp = temperatura();
	pH10 = temp*0,002+9,96;
	valor_pH10 = pH(temp);

	temp = temperatura();
	pH7 = temp*0,002+6,96;
	valor_pH7 = pH(temp);

	span_pH = (valor_pH10-valor_pH7)/(pH10-pH7);
	ZERO_pH = (span_pH*pH7)-valor_pH7;

}

*/
