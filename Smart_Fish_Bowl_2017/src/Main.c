/*
===============================================================================
 Name        : main.c
 Author      : Gerardo Zoffoli
 Version     :
 Copyright   : Copyright (C)
 Description : main definition
===============================================================================
*/

#ifdef __USE_CMSIS
#include "LPC17xx.h"
#endif

#include <cr_section_macros.h>
#include <NXP/crp.h>
#include <string.h>

// TODO: insert other include files here
/*Includes del FreeRTOS*/
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

/*Includes de la C-API*/
#include "GPIO.h"

/*Includes de la carpeta /inc */
#include "TECLADO.h"
#include "TECLADO.c"
#include "PANTALLA.h"
#include "AD7793.h"
#include "MENUS.h"
#include "RELOJ.h"
#include "TARJETA_SD.h"


// TODO: insert other definitions and declarations here
GPIO Columna1;
GPIO Columna2;
GPIO Columna3;
GPIO Columna4;
GPIO Fila1;
GPIO Fila2;
GPIO Fila3;
GPIO Fila4;
GPIO ledStick;

//Variables externas
extern xQueueHandle ColaTECLADO;
extern xQueueHandle ColaSD;
extern xSemaphoreHandle	SemaforoDeConteo1Seg;

//==============================================================================================
//		-		Proposito: Inicializacion general del sistema
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================
void SistemInit(void)
{
	//Incialización del Hardware

	//InitRTCtoDefault();
	ForceRTCtoSpecific();
	GPIO_Init(&Fila4,SALIDA,BAJO,(int)GPIO__2_10);
	GPIO_Init(&Fila3,SALIDA,BAJO,(int)GPIO__2_13);
	GPIO_Init(&Fila2,SALIDA,BAJO,(int)GPIO__0_28);
	GPIO_Init(&Fila1,SALIDA,BAJO,(int)GPIO__0_27);
	GPIO_Init(&Columna4,ENTRADA,BAJO,(int)GPIO__0_26);
	GPIO_Init(&Columna3,ENTRADA,BAJO,(int)GPIO__0_25);
	GPIO_Init(&Columna2,ENTRADA,BAJO,(int)GPIO__0_24);
	GPIO_Init(&Columna1,ENTRADA,BAJO,(int)GPIO__0_23);
	GPIO_Init(&ledStick, SALIDA, ALTO, (int)GPIO__0_22); // eliminar en conjunto con la función ToggleLed

	Pasivar(&Columna1);
	Pasivar(&Columna2);
	Pasivar(&Columna3);
	Pasivar(&Columna4);

	LCDtextInit(&DisplayLCD, FILAS, COLUMNAS, (int)GPIO__2_5, (int)GPIO__2_6, (int)GPIO__2_7, (int)GPIO__2_8, (int)GPIO__2_0, (int)GPIO__2_1);

	//Inicializacion de los menus del sistema
	CreardorMenus();

	//Inicializacion del AD
	AD7793_Init();

}

//==============================================================================================
//		-		Proposito: Tarea que indica rapidamente si el sistema sigue corriendo
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================
void TareaLED (void *pvParameters)
{
	portTickType xMeDesperte;
	xMeDesperte = xTaskGetTickCount();

	while(1)
	{
		if(isActivo(&ledStick))
		{
			Pasivar(&ledStick);
		}
		else
		{
			Activar(&ledStick);
		}

		vTaskDelayUntil(&xMeDesperte, 1000/portTICK_RATE_MS);
	}
}


//==============================================================================================
//		-		Proposito: Main del proyecto
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================
int main(void) {

	SistemInit();

	//Creación de las tareas
	xTaskCreate(TareaMenu,
			    ( signed portCHAR * )"Menu",
				configMINIMAL_STACK_SIZE*5,
				NULL,
				tskIDLE_PRIORITY+2,
				NULL );

	xTaskCreate(TareaTecla,
			    ( signed portCHAR * )"Tecl",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY+1,
				NULL );

	xTaskCreate(TareaLCD,
			    ( signed portCHAR * )"LCD",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY+1,
				NULL);

	xTaskCreate(TareaAutoDispatcher,
			    "AutoDisp",
				configMINIMAL_STACK_SIZE*5,
				NULL,
				tskIDLE_PRIORITY+1,
				NULL);

	xTaskCreate(TareaLED,
			    "LED",
				configMINIMAL_STACK_SIZE,
				NULL,
				tskIDLE_PRIORITY+1,
				NULL);

	xTaskCreate(
				TareaRegistrar,
				( signed portCHAR * ) "REG",
				configMINIMAL_STACK_SIZE*5,
				NULL,
				tskIDLE_PRIORITY+3,
				NULL );


	//Inicio de las colas de mensajes
	ColaTECLADO = xQueueCreate( 3, sizeof( unsigned int ) );
	ColaSD = xQueueCreate( 3, sizeof( SDdata ) );

	//Inicio el Scheduler
	vTaskStartScheduler();

	// Enter an infinite loop, just incrementing a counter
	volatile static int i = 0 ;
	while(1) {
		i++ ;
	}
	return 0 ;
}




//==============================================================================================
//		-		Observacion: Por el momento no utilizamos las siguientes funciones de RTOS
//==============================================================================================
void vApplicationTickHook ( void )
{

}

void vApplicationIdleHook ( void )
{
	__ASM volatile ("wfe");
}

