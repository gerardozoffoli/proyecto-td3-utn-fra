/*
 * RELOJ.c
 *
 *  Created on: /08/2017
 *      Author: Gerardo
 */

#include "RELOJ.h"
#include "PANTALLA.h"
#include "menus.h"

// TODO: insert other definitions and declarations here
xSemaphoreHandle	SemaforoDeConteo1Seg;
xTaskHandle xHandle;

// Cola de mensajes
//extern xQueueHandle ColaLCD;

#define TRES				3
#define SEIS				6
#define PRIMERA_VEZ			0
#define PRINCIPIO			1
#define FIN					8

char Str_MsgH[16];
char Str_MsgF[16];
char FlagData = NULL;

//==============================================================================================
//		-		Proposito: Funcion de atencion a interrupcion por conteo x SEG del RTC
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================

void Counting(void){

	portBASE_TYPE xHigherPriorityTaskWoken = pdFALSE;
	xSemaphoreGiveFromISR(SemaforoDeConteo1Seg, &xHigherPriorityTaskWoken);
	portEND_SWITCHING_ISR(xHigherPriorityTaskWoken);
	return;
}


/*//==============================================================================================
//		-		Proposito: Funcion para obtener un string con la fecha en formato: AAAAMMDD
//		-		Parametros: void
//		-		Retorna: char*
//==============================================================================================

const char* GetDateStringFromRTC (void)
{
	char Str_Msg[16];
	char aux;
	RTCDate* fecha = NULL;

	RTC_GetFullTime(LPC_RTC, (RTC_TIME_Type*) fecha);

    Str_Msg[0] = (fecha->year/1000)+48;
    aux = fecha->year%1000;
    Str_Msg[1] = (aux/100)+48 ;
    aux = fecha->year%100;
    Str_Msg[2] = (aux/10)+48;
    Str_Msg[3] = (aux%10)+48;
    Str_Msg[4] = (fecha->month/10)+48;
    Str_Msg[5] = (fecha->month%10)+48;
    Str_Msg[6] = (fecha->dayofmonth/10)+48;
    Str_Msg[7] = (fecha->dayofmonth%10)+48;
    Str_Msg[8] = '\0';

    return Str_Msg;
}


//==============================================================================================
//		-		Proposito: Funcion para obtener un string con la fecha en formato: HHMMSS
//		-		Parametros: void
//		-		Retorna: char*
//==============================================================================================

const char* GetTimeStringFromRTC (void)
{
    char Str_Msg[16];
    RTCDate* fecha = NULL;

    RTC_GetFullTime(LPC_RTC, (RTC_TIME_Type*) fecha);

    Str_Msg[0] = (fecha->hours/10)+48;
    Str_Msg[1] = (fecha->hours%10)+48;
    Str_Msg[2] = (fecha->minutes/10)+48;
    Str_Msg[3] = (fecha->minutes%10)+48;
    Str_Msg[4] = (fecha->seconds/10)+48;
    Str_Msg[5] = (fecha->seconds%10)+48;
    Str_Msg[6] = '\0';

    return Str_Msg;

}
*/

//==============================================================================================
//		-		Proposito: Funcion para setear hora a partir de string en formato: HHMMSS
//		-		Parametros: char*
//		-		Retorna: void
//==============================================================================================

void SetTimeStringtoRTC (char* str)
{
    int horas, minutos, segundos;

    horas = ((str[0] - '0')*10) + (str[1] - '0');
    minutos = ((str[3] - '0')*10) + (str[4] - '0');
    segundos = ((str[6] - '0')*10) + (str[7] - '0');

    RTC_SetTime(LPC_RTC, RTC_TIMETYPE_HOUR, horas);
    RTC_SetTime(LPC_RTC, RTC_TIMETYPE_MINUTE, minutos);
    RTC_SetTime(LPC_RTC, RTC_TIMETYPE_SECOND, segundos);

}


//==============================================================================================
//		-		Proposito: Funcion para setear fecha a partir de string en formato: DDMMAA
//		-		Parametros: char*
//		-		Retorna: void
//==============================================================================================

void SetDateStringtoRTC (char* str)
{
    uint32_t dia, mes, ano;

    dia = ((str[0] - '0')*10) + (str[1] - '0');
    mes = ((str[3] - '0')*10) + (str[4] - '0');
    ano = ((str[6] - '0')*10) + (str[7] - '0');

    RTC_SetTime(LPC_RTC, RTC_TIMETYPE_DAYOFMONTH, dia);
    RTC_SetTime(LPC_RTC, RTC_TIMETYPE_MONTH, mes);
    RTC_SetTime(LPC_RTC, RTC_TIMETYPE_YEAR, ano);

}

//==============================================================================================
//		-		Proposito: Refresca Fecha/Hora en el LCD para mostrar al usuario
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================

/*void TareaRELOJ (void *pvParameters)
{

	uint8_t cont;

	    while (1)
		{

	    	if(cont%2)
				EventDispatchMenu( HORA);
			else
				EventDispatchMenu( FECHA);

			cont++;

			vTaskDelay(portTICK_RATE_MS * 1000);
		}

}
*/

//==============================================================================================
//		-		Proposito: Inicializacion de RTC en una fecha/hora default
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================
void InitRTCtoDefault (void)
{
	RTCDate fecha_actual;

	//Inicio del sistema, elección arbitraria
	fecha_actual.seconds = 0;
	fecha_actual.minutes = 0;
	fecha_actual.hours = 0;
	fecha_actual.dayofweek = 1;
	fecha_actual.dayofmonth = 01;
	fecha_actual.dayofyear = 1;
	fecha_actual.month = 01;
	fecha_actual.year = 2000;

	RTCset_date(&fecha_actual, Counting);

}


//==============================================================================================
//		-		Proposito: Inicializacion de RTC en una fecha/hora forzada por usuario
//		-		Parametros: void
//		-		Retorna: void
//==============================================================================================
void ForceRTCtoSpecific (void)
{
	RTCDate fecha_actual;

	//Inicio del sistema, elección forzada por el user
	fecha_actual.seconds = 0;
	fecha_actual.minutes = 53;
	fecha_actual.hours = 20;
	fecha_actual.dayofweek = 1;
	fecha_actual.dayofmonth = 03;
	fecha_actual.dayofyear = 1;
	fecha_actual.month = 12;
	fecha_actual.year = 2015;

	RTCset_date(&fecha_actual, Counting);

}



void BlinkFechaHora(Eventos EventId, char param2)
{

	static int blink_flag = 0;
	RTCDate fecha;
	char aux;
	char Str_MsgH[16];
	char Str_MsgF[16];

	RTC_GetFullTime(LPC_RTC, (RTC_TIME_Type*) &fecha);

	Str_MsgH[0] = (fecha.hours/10)+48;
	Str_MsgH[1] = (fecha.hours%10)+48;
	Str_MsgH[2] = ':';
	Str_MsgH[3] = (fecha.minutes/10)+48;
	Str_MsgH[4] = (fecha.minutes%10)+48;
	Str_MsgH[5] = ':';
	Str_MsgH[6] = (fecha.seconds/10)+48;
	Str_MsgH[7] = (fecha.seconds%10)+48;
	Str_MsgH[8] = '\0';

    Str_MsgF[0] = (fecha.dayofmonth/10)+48;
    Str_MsgF[1] = (fecha.dayofmonth%10)+48;
    Str_MsgF[2] = '/';
    Str_MsgF[3] = (fecha.month/10)+48;
    Str_MsgF[4] = (fecha.month%10)+48;
    Str_MsgF[5] = '/';
    aux = fecha.year%1000;
    aux = fecha.year%100;
    Str_MsgF[6] = (aux/10)+48;
    Str_MsgF[7] = (aux%10)+48;
    Str_MsgF[8] = '\0';

	if(blink_flag)
	{
		LCDtextGotoXY(&DisplayLCD,0, 1);
		LCDtextPuts(&DisplayLCD, Str_MsgH);
		blink_flag = 0;
	}
	else
	{
		LCDtextGotoXY(&DisplayLCD,0, 1);
	    LCDtextPuts(&DisplayLCD, Str_MsgF);
	    blink_flag = 1;
	}

}



void SeteaDataHora (Eventos EventId, char param2)
{
	RTCDate fecha;
	static unsigned int INDICE= 0;
	static char Str_HORA[9]={"HH:MM:SS"};

	if ( (param2>=1) && (param2<=9) && (EventId == HORA) )

	{
		if (INDICE==PRIMERA_VEZ)
		{
			LCDtextClear(&DisplayLCD);
			LCDtextGotoXY(&DisplayLCD,0, 0);
			LCDtextPuts(&DisplayLCD, Str_HORA);
			//LCDtextBlinking(&DisplayLCD, true);
			INDICE++;
			LCDtextGotoXY(&DisplayLCD,0, 0);
		}
		if( (INDICE >=PRINCIPIO) && (INDICE <=FIN) )
		{
			if ( (INDICE == TRES) || (INDICE == SEIS) ) INDICE++;
			Str_HORA [(INDICE-1)]= param2+47;
			LCDtextGotoXY(&DisplayLCD,0, 0);
			LCDtextPuts(&DisplayLCD, Str_HORA);
			LCDtextGotoXY(&DisplayLCD,INDICE, 0);
			INDICE++;
		}

		if( (INDICE>FIN+1) && ( 10 == param2))
		{

			//RTC_SetFullTime(LPC_RTC, &fecha);
			INDICE= PRINCIPIO;
			strcpy(Str_HORA, "HH:MM:SS");
			//LCDtextBlinking(&DisplayLCD, false);

		}
	}
}

void SeteaDataFecha (Eventos EventId, char param2)
{
	RTCDate fecha;
	static unsigned int INDICE= 0;
	static char Str_FECHA[9]={"DD/MM/AA"};

	if ( (param2>=1) && (param2<=9) && (EventId == FECHA) )

	{
		if (INDICE==PRIMERA_VEZ)
		{
			LCDtextClear(&DisplayLCD);
			LCDtextGotoXY(&DisplayLCD,0, 0);
			LCDtextPuts(&DisplayLCD, Str_FECHA);
			INDICE++;
			LCDtextGotoXY(&DisplayLCD,0, 0);
		}
		if( (INDICE >=PRINCIPIO) && (INDICE <=FIN) )
		{
			if ( (INDICE == TRES) || (INDICE == SEIS) ) INDICE++;
			Str_FECHA [(INDICE-1)]= param2+47;
			LCDtextGotoXY(&DisplayLCD,0, 0);
			LCDtextPuts(&DisplayLCD, Str_FECHA);
			LCDtextGotoXY(&DisplayLCD,INDICE, 0);
			INDICE++;
		}

		if( (INDICE>FIN+1) && ( 10 == param2))
		{

			//RTC_SetFullTime(LPC_RTC, &fecha);
			INDICE= PRINCIPIO;
			strcpy(Str_FECHA, "DD/MM/AA");

		}
	}
}
