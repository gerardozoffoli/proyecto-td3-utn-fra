/*
 * PANTALLA.c
 *
 *  Created on: /08/2017
 *      Author: Gerardo
 */

#include "PANTALLA.h"



void TareaLCD(void *pvParameters)
{
	LCD_Status ret;
	portTickType delay;
	while(1)
	{
		ret = LCDworker(&DisplayLCD);
		switch(ret)
		{
			case WAIT_CMD:
				delay = portTICK_RATE_MS * 20;
				break;
			case WAIT_SCREEN:
				delay = 2/portMAX_DELAY;
				break;
			case NOTHING_TODO:
				break;
		}
		vTaskDelay(delay);
	}
}


//==============================================================================================
//		-		Proposito: Tarea que escribe en el LCD de acuerdo a lo recibido en una cola
//		-		Parametros: void
//		-		Retorna: void
//		-		Observaciones: Fila 0 --> Fila superior || Fila 1 --> Fila inferior
//==============================================================================================

/* void TareaLCD (void *pvParameters)
{

	LCDMsg msg;
	int i;

	while(1)
	{
		if(xQueueReceive(ColaLCD, &msg, portMAX_DELAY)==pdTRUE)
		{
			LCDtextClear(&DisplayLCD);

			if(msg.fila==0)
			{
				LCDtextGotoXY(&DisplayLCD, 0, 0);
				LCDtextPuts(&DisplayLCD,msg.mensaje);
				for(i=0;i<strlen(msg.mensaje);i++)
				{
					LCDworker(&DisplayLCD);
					vTaskDelay(2);
				}
			}
			else
			{
				LCDtextGotoXY(&DisplayLCD, 0, 1);
				LCDtextPuts(&DisplayLCD,msg.mensaje);
				for(i=0;i<strlen(msg.mensaje);i++)
				{
					LCDworker(&DisplayLCD);
					vTaskDelay(2);

				}
			}
		}
	}

}

*/






