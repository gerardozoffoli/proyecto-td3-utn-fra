################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Lib_FAT_ElmChanFS/src/CAPI_capa_SPI.c \
../Lib_FAT_ElmChanFS/src/CAPI_diskio.c \
../Lib_FAT_ElmChanFS/src/extras.c \
../Lib_FAT_ElmChanFS/src/ff.c \
../Lib_FAT_ElmChanFS/src/zzzCAPI_SPI_Layer.c 

OBJS += \
./Lib_FAT_ElmChanFS/src/CAPI_capa_SPI.o \
./Lib_FAT_ElmChanFS/src/CAPI_diskio.o \
./Lib_FAT_ElmChanFS/src/extras.o \
./Lib_FAT_ElmChanFS/src/ff.o \
./Lib_FAT_ElmChanFS/src/zzzCAPI_SPI_Layer.o 

C_DEPS += \
./Lib_FAT_ElmChanFS/src/CAPI_capa_SPI.d \
./Lib_FAT_ElmChanFS/src/CAPI_diskio.d \
./Lib_FAT_ElmChanFS/src/extras.d \
./Lib_FAT_ElmChanFS/src/ff.d \
./Lib_FAT_ElmChanFS/src/zzzCAPI_SPI_Layer.d 


# Each subdirectory must supply rules for building sources it contributes
Lib_FAT_ElmChanFS/src/%.o: ../Lib_FAT_ElmChanFS/src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"D:\PHMetro_UTN_2015\Lib_FAT_ElmChanFS\inc" -I"D:\PHMetro_UTN_2015\CMSISv2p00_LPC17xx\inc" -I"D:\PHMetro_UTN_2015\Lib_Contenedores\inc" -I"D:\PHMetro_UTN_2015\Lib_MCU_LPC176x\vendor drivers\inc" -I"D:\PHMetro_UTN_2015\Lib_MCU_LPC176x\inc" -I"D:\PHMetro_UTN_2015\Lib_API_CAPI\inc" -I"D:\PHMetro_UTN_2015\Lib_FAT_ElmChanFS\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


