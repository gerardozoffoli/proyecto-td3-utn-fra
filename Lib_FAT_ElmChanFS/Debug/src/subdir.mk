################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../src/CAPI_capa_SPI.c \
../src/CAPI_diskio.c \
../src/extras.c \
../src/ff.c 

OBJS += \
./src/CAPI_capa_SPI.o \
./src/CAPI_diskio.o \
./src/extras.o \
./src/ff.o 

C_DEPS += \
./src/CAPI_capa_SPI.d \
./src/CAPI_diskio.d \
./src/extras.d \
./src/ff.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: MCU C Compiler'
	arm-none-eabi-gcc -D__REDLIB__ -DDEBUG -D__CODE_RED -I"C:\TD3\proyecto-td3-utn-fra\Lib_FAT_ElmChanFS\inc" -I"C:\TD3\proyecto-td3-utn-fra\CMSISv2p00_LPC17xx\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_Contenedores\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_MCU_LPC176x\vendor drivers\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_MCU_LPC176x\inc" -I"C:\TD3\proyecto-td3-utn-fra\Lib_API_CAPI\inc" -O0 -g3 -Wall -c -fmessage-length=0 -fno-builtin -ffunction-sections -fdata-sections -mcpu=cortex-m3 -mthumb -D__REDLIB__ -specs=redlib.specs -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@:%.o=%.o)" -MT"$(@:%.o=%.d)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


